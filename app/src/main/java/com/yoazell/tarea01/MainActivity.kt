package com.yoazell.tarea01

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.core.view.isVisible
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        fun View.VisibleOrGone(visible: Boolean){
            visibility = if (visible)  View.VISIBLE  else  View.GONE
        }

        btnProcesar.setOnClickListener {
            val ls_anio = edtAnio.text.toString()

            if ( ls_anio.isEmpty() ){
                Toast.makeText(this, "Debe ingresar el año de nacimiento", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            val li_anio = ls_anio.toString().toInt()
            var lsRptaUno = "0"

            when (li_anio){
                in 1994..2010 -> {lsRptaUno = "7.800.000"
                                  txtRptaDosIrrev.VisibleOrGone(true)
                                  txtRptaDosFrust.VisibleOrGone(false)
                                  txtRptaDosObsExito.VisibleOrGone(false)
                                  txtRptaDosAmb.VisibleOrGone(false)
                                  txtRptaDosAust.VisibleOrGone(false)
                }
                in 1981..1993 -> {  lsRptaUno = "7.200.000"
                                    txtRptaDosIrrev.VisibleOrGone(false)
                                    txtRptaDosFrust.VisibleOrGone(true)
                                    txtRptaDosObsExito.VisibleOrGone(false)
                                    txtRptaDosAmb.VisibleOrGone(false)
                                    txtRptaDosAust.VisibleOrGone(false)
                }
                in 1969..1980 -> {lsRptaUno = "9.300.000"
                                    txtRptaDosIrrev.VisibleOrGone(false)
                                    txtRptaDosFrust.VisibleOrGone(false)
                                    txtRptaDosObsExito.VisibleOrGone(true)
                                    txtRptaDosAmb.VisibleOrGone(false)
                                    txtRptaDosAust.VisibleOrGone(false)
                }
                in 1949..1968 -> {lsRptaUno = "12.200.000"
                                    txtRptaDosIrrev.VisibleOrGone(false)
                                    txtRptaDosFrust.VisibleOrGone(false)
                                    txtRptaDosObsExito.VisibleOrGone(false)
                                    txtRptaDosAmb.VisibleOrGone(true)
                                    txtRptaDosAust.VisibleOrGone(false)
                }
                in 1930..1948 -> {lsRptaUno = "6.300.000"
                                    txtRptaDosIrrev.VisibleOrGone(false)
                                    txtRptaDosFrust.VisibleOrGone(false)
                                    txtRptaDosObsExito.VisibleOrGone(false)
                                    txtRptaDosAmb.VisibleOrGone(false)
                                    txtRptaDosAust.VisibleOrGone(true)
                }
                else -> {
                    lsRptaUno = "No contamos con información"
                    txtRptaDosIrrev.VisibleOrGone(false)
                    txtRptaDosFrust.VisibleOrGone(false)
                    txtRptaDosObsExito.VisibleOrGone(false)
                    txtRptaDosAmb.VisibleOrGone(false)
                    txtRptaDosAust.VisibleOrGone(false)
                }
            }

            txtRptaUno.text = lsRptaUno

        }
    }
}
